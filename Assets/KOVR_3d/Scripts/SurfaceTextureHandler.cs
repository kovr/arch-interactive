﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SurfaceTextureHandler : MonoBehaviour {

    public GameObject gazeCamera;
    public GameObject sampleSelectorPrefab;
    public GameObject realtyBot;
    public float sampleHeight;
    private SampleSelector selectFunctions;
    private GameObject oldSampleSelector;
    private SurfaceCollider activeSurfaceCollider;

	// Use this for initialization
	void Start () {
        
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void ClassifyResult(string result)
    {
        if(gazeCamera != null)
        {
            Vector3 ray = Camera.main.transform.forward;
            RaycastHit hit;
            if (Physics.Raycast(Camera.main.transform.position, ray, out hit))
            {
                if ((result == "paint" || result == "counters") && hit.collider.tag == "EditableSurface")
                {
                    ShowSamples(hit);
                }

            }
        }
    }


    public void ShowSamples(RaycastHit hit)
    {
        SurfaceCollider sc = hit.collider.gameObject.GetComponent<SurfaceCollider>();
        if (sc != null && sc != activeSurfaceCollider)
        {
            activeSurfaceCollider = sc;
            if (oldSampleSelector != null)
            {
                Destroy(oldSampleSelector);
                oldSampleSelector = null;
            }

            GameObject sampleSelector = Instantiate<GameObject>(sampleSelectorPrefab);
            oldSampleSelector = sampleSelector;
            selectFunctions = sampleSelector.GetComponent<SampleSelector>();
            selectFunctions.SetPalette(sc.myPalette);

            sampleSelector.transform.parent = realtyBot.transform;
            sampleSelector.transform.localPosition = new Vector3(0, sampleHeight, 0);
            sampleSelector.GetComponent<SampleSelector>().playerHead = gazeCamera;
            realtyBot.SendMessage("SetTarget", hit.point);
            realtyBot.SendMessage("SetSamples", sampleSelector.GetComponent<SampleSelector>());
        }
    }

    public void AssignSelection(RaycastHit hit)
    {
        if(activeSurfaceCollider != null)
        {
            activeSurfaceCollider.SelectSwatchByName(hit.collider.name);
        }
    }

}
