﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SampleSelector : MonoBehaviour {

    public GameObject samplePrefab;
    public GameObject playerHead;
    public float tileRadius;

    private SurfacePalette activePalette;

    public void SetPalette(SurfacePalette newP)
    {
        activePalette = newP;
        foreach (Transform t in transform)
        {
            Destroy(t.gameObject);
        }

        int idx = 0;
        float spacing = 360 / activePalette.surfaceProperties.Length;
        foreach(SurfacePalette.SurfaceProperty sp in activePalette.surfaceProperties)
        {
            GameObject tile = Instantiate<GameObject>(samplePrefab);
            MaterialChip mc = tile.GetComponent<MaterialChip>();
            tile.transform.parent = transform;
            tile.name = sp.name;

            float ang = idx * spacing;
            Vector3 pos;
            pos.x = tileRadius * Mathf.Sin(ang * Mathf.Deg2Rad);
            pos.y = tileRadius * Mathf.Cos(ang * Mathf.Deg2Rad);
            pos.z = 0;

            tile.transform.localPosition = pos;

            if(sp.material != null)
            {
                mc.ApplyTexture(sp.name, sp.material, idx);
            }
            else if (sp.texture != null)
            {

                Material nm = new Material(Shader.Find("Standard"));
                nm.mainTexture = sp.texture;
                nm.color = sp.color;
                mc.ApplyTexture(sp.name, nm, idx);
            }
            else
            {
                Material nm = new Material(Shader.Find("Standard"));
                nm.color = sp.color;
                mc.ApplyTexture(sp.name, nm, idx);
            }

            idx++;
        }

    }


	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (playerHead != null)
        {
            transform.LookAt(playerHead.transform);
        }
        
	}
}
