﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MaterialChip : MonoBehaviour {

    private TextMesh myTextMesh;
    private MeshRenderer myMesh;
    public int myIndex;
    public Material myMaterial;
    public string myName;

	// Use this for initialization
	void Start () {

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void FindMaterial()
    {
        Transform mn = transform.Find("ChipMesh");
        if (mn != null)
        {
            myMesh = mn.gameObject.GetComponent<MeshRenderer>();
        }
    }

    public void FindTextMesh()
    {
        Transform cm = transform.Find("MaterialName");
        if (cm != null)
        {
            myTextMesh = cm.gameObject.GetComponent<TextMesh>();
        }
    }

    public void ApplyTexture(string materialName, Material newMaterial, int newIndex)
    {
        FindMaterial();
        if (myMesh != null)
        {
            myMesh.material = newMaterial;
        }
        FindTextMesh();
        if (myTextMesh != null)
        {
            myTextMesh.text = materialName;
        }
        myIndex = newIndex;
    }


}
