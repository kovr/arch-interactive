﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateClosestTo : MonoBehaviour {

    public GameObject trackedObject;
    public GameObject[] activatorObjects;

    public float checkCycleMax = 5;
    private float currentCycle = 0;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        currentCycle -= Time.deltaTime;
        if(currentCycle <= 0)
        {
            ActivateClosest();
            currentCycle = checkCycleMax;
        }
    }

    public void ActivateClosest()
    {
        if (trackedObject != null)
        {
            GameObject minObj = null;
            float closestDist = 100f;
            for (int i = 0; i < activatorObjects.Length; i++)
            {
                float dist = Vector3.Distance(trackedObject.transform.position, activatorObjects[i].transform.position);
                if (dist < closestDist)
                {
                    minObj = activatorObjects[i];
                    closestDist = dist;
                }
            }

            for (int i = 0; i < activatorObjects.Length; i++)
            {
                activatorObjects[i].SetActive(activatorObjects[i] == minObj);
            }
            
        }
    }

}
