﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SurfacePalette : MonoBehaviour {

    public SurfaceCollider.SurfaceType myType;
    public SurfaceProperty[] surfaceProperties;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    [System.Serializable]
    public class SurfaceProperty
    {
        public string name;
        public Texture2D texture;
        public Material material;
        public Color32 color;
    }
}
