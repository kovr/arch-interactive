﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtPlayer : MonoBehaviour {


    public GameObject playerFace;
    public float lookOffset;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        transform.LookAt(new Vector3(playerFace.transform.position.x, playerFace.transform.position.y + lookOffset, playerFace.transform.position.z));
	}
}
