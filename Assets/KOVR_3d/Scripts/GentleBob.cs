﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GentleBob : MonoBehaviour {

    private float cycle;
    private float initY;
    public float spd;
    public float deltaPos;


	// Use this for initialization
	void Start () {
        cycle = 0;
        initY = transform.position.y;
    }
	
	// Update is called once per frame
	void Update () {
        cycle += spd * Time.deltaTime;
        float adj = Mathf.Sin(cycle) * deltaPos;
        transform.position = new Vector3(transform.position.x, initY + adj, transform.position.z);
	}
}
