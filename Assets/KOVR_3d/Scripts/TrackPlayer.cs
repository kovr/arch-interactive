﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrackPlayer : MonoBehaviour {

    public GameObject trackObject;
    public bool matchRotationY;

    private Vector3 initEulerAngles;

	// Use this for initialization
	void Start () {
        initEulerAngles = transform.eulerAngles;

    }
	
	// Update is called once per frame
	void Update () {
		if(trackObject != null)
        {
            transform.position = new Vector3(trackObject.transform.position.x, transform.position.y, trackObject.transform.position.z);
            if (matchRotationY)
            {
                transform.eulerAngles = new Vector3(initEulerAngles.x, trackObject.transform.eulerAngles.y, initEulerAngles.z);
            }
        }
	}
}
