﻿using IBM.Watson.DeveloperCloud.Widgets;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class ActivateOnSteamButton : MonoBehaviour {

    public Hand controller1;
    public Hand controller2;

    public Valve.VR.EVRButtonId triggerButton;
    public bool useGrip;
    public GameObject cdw;

    // Use this for initialization
    void Start () {
        if (useGrip)
        {
            triggerButton = Valve.VR.EVRButtonId.k_EButton_Grip;
        }
	}
	
	// Update is called once per frame
	void Update () {
        if (controller1 != null)
        {
            SteamVR_LaserPointer ls = controller1.gameObject.GetComponent<SteamVR_LaserPointer>();
            if (ls != null)
            {
                if(controller1 != null && controller1.controller != null)
                {
                    if (controller1.controller.GetPress(triggerButton))
                    {
                        cdw.SetActive(true);
                    }
                    else
                    {
                        cdw.SetActive(false);
                    }
                }
            }
        }
        if (controller2 != null)
        {
            SteamVR_LaserPointer rs = controller2.gameObject.GetComponent<SteamVR_LaserPointer>();
            if (rs != null)
            {
                if (controller1 != null && controller1.controller != null)
                {
                    if (controller2 != null && controller2.controller.GetPress(triggerButton))
                    {
                        cdw.SetActive(true);
                    }
                    else
                    {
                        cdw.SetActive(false);
                    }
                }
            }
        }
    }
}
