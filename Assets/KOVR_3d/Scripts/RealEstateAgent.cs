﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class RealEstateAgent : MonoBehaviour {

    public NavMeshAgent myAgent;
    public GameObject walkTarget;
    public float minMoveDistance;

    private Vector3 currentTarget;
    private SampleSelector currentSelector;
    private NavMeshAgent nma;

	// Use this for initialization
	void Start () {
        currentTarget = new Vector3(transform.position.x,transform.position.y,transform.position.z);
        nma = GetComponent<NavMeshAgent>();
    }
	
	// Update is called once per frame
	void Update () {
        if (nma.hasPath)
        {
            if (currentSelector != null)
            {
                currentSelector.gameObject.SetActive(false);
            }
        }
        else
        {
            if (currentSelector != null)
            {
                currentSelector.gameObject.SetActive(true);
            }
        }
        if(walkTarget != null)
        {
            float currentDist = Vector3.Distance(walkTarget.transform.position, currentTarget);
            if (currentDist > minMoveDistance)
            {
                SetTarget(walkTarget.transform.position);
                myAgent.SetDestination(currentTarget);
            }

            //float botDistToTarget = Vector3.Distance(transform.position, currentTarget);
            //if (currentDist > 0.5f)
            //{
            //    if (currentSelector != null)
            //    {
            //        currentSelector.gameObject.SetActive(false);
            //    }
            //}
            //else
            //{
            //    if (currentSelector != null)
            //    {
            //        currentSelector.gameObject.SetActive(true);
            //    }
            //}
        }
        
	}

    public void SetSamples(SampleSelector s)
    {
        currentSelector = s;
    }



    public void SetTarget(Vector3 newTarget)
    {
        currentTarget = newTarget;
    }

}
