﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SurfaceCollider : MonoBehaviour {

    public enum SurfaceType { Wall, Counter, Floor };
    public MeshRenderer targetMesh;
    public SurfacePalette myPalette;
    

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private int currentSwatch = 0;
    public void SelectSwatch(int swatchNumber = 0)
    {
        if (swatchNumber == -1)
        {
            currentSwatch = (int)Mathf.Repeat(currentSwatch + 1, myPalette.surfaceProperties.Length);
            targetMesh.material.color = myPalette.surfaceProperties[currentSwatch].color;
            targetMesh.material.mainTexture = myPalette.surfaceProperties[currentSwatch].texture;
        }

    }

    public void SelectSwatchByName(string swatchName)
    {
        int idx = 0;
        foreach (SurfacePalette.SurfaceProperty sp in myPalette.surfaceProperties)
        {
            if(sp.name.ToLower() == swatchName.ToLower())
            {
                currentSwatch = idx;
                if (myPalette.surfaceProperties[currentSwatch].material != null)
                {
                    targetMesh.material = myPalette.surfaceProperties[currentSwatch].material;
                    for (int i = 0; i < targetMesh.materials.Length; i++)
                    {
                        targetMesh.materials[i] = myPalette.surfaceProperties[currentSwatch].material;
                    }
                }
                else
                {
                    targetMesh.material.color = myPalette.surfaceProperties[currentSwatch].color;
                    targetMesh.material.mainTexture = myPalette.surfaceProperties[currentSwatch].texture;
                    for (int i = 0; i < targetMesh.materials.Length; i++)
                    {
                        targetMesh.materials[i].color = myPalette.surfaceProperties[currentSwatch].color;
                        targetMesh.materials[i].mainTexture = myPalette.surfaceProperties[currentSwatch].texture;
                    }
                }

            }
            idx++;
        }
    }

    public void ShowSwatches()
    {
       
    }



}
