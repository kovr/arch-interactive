﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR;
using IBM.Watson.DeveloperCloud;
using IBM.Watson.DeveloperCloud.Services.SpeechToText.v1;
using IBM.Watson.DeveloperCloud.Services.NaturalLanguageClassifier.v1;
using UnityEngine.UI;
using Valve.VR.InteractionSystem;
using System;

public class PlayerManager : MonoBehaviour {

    public Text debugText;
    public AudioByClass[] audioClips;
    public SurfaceTextureHandler surfaceHandler;

    public Hand controller1;
    public Hand controller2;

    private AudioClip m_AudioClip = new AudioClip();
    private SpeechToText m_SpeechToText = new SpeechToText();

    // Use this for initialization
    void Start () {
        
    }
	
	// Update is called once per frame
	void Update () {
        if(controller1 != null)
        {
            SteamVR_LaserPointer ls = controller1.gameObject.GetComponent<SteamVR_LaserPointer>();
            if (ls != null)
            {
                if(controller1 != null && controller1.controller != null)
                {
                    if (controller1.controller.GetPress(Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger))
                    {
                        ls.active = true;
                        ls.thickness = 0.02f;
                        Vector3 ray = ls.transform.forward;
                        RaycastHit hit;
                        if (Physics.Raycast(controller1.transform.position, ray, out hit))
                        {
                            Debug.Log(hit.collider.gameObject);
                        }
                    }
                    else
                    {
                        ls.active = false;
                        ls.thickness = 0f;
                    }
                }

            }
        }
        if (controller2 != null)
        {
            SteamVR_LaserPointer ls = controller2.gameObject.GetComponent<SteamVR_LaserPointer>();
            if (ls != null)
            {
                if(controller2 != null && controller2.controller != null)
                {
                    if (controller2.controller.GetPress(Valve.VR.EVRButtonId.k_EButton_SteamVR_Trigger))
                    {
                        ls.active = true;
                        ls.thickness = 0.002f;

                        Vector3 ray = ls.transform.forward;
                        RaycastHit hit;
                        if (Physics.Raycast(controller2.transform.position, ray, out hit))
                        {
                            HandleBeamCollision(hit);
                        }
                    }
                    else
                    {
                        ls.active = false;
                        ls.thickness = 0f;
                    }
                }
            }
        }

    }

    private void HandleBeamCollision(RaycastHit hit)
    {
        switch (hit.collider.tag) {

            case "EditableSurface":
                surfaceHandler.ShowSamples(hit);
                break;

            case "Material":
                surfaceHandler.AssignSelection(hit);
                break;

        }
    }

    private void ClassifyResult(string result)
    {
        Debug.Log("==========|| " + result);

        for (int i = 0; i < audioClips.Length; i++)
        {
            if (audioClips[i].relatedClass.ToLower() == result.ToLower())
            {
                audioClips[i].clip.Play();
                break;
            }
        }
    }


}

[System.Serializable]
public class AudioByClass
{
    public string relatedClass;
    public AudioSource clip;
}